# README
----------------
This is a simple Android project designed to be part of a bigger project.  
It is the base for a drag and drop puzzle, where you pull parts which can clip into the grid if there is room, or can go back to their initial place if there is not.

For more information on how to install, see the [wiki](https://bitbucket.org/OroshiX/drag-and-drop-puzzle/wiki/Home).