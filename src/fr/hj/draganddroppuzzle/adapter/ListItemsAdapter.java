package fr.hj.draganddroppuzzle.adapter;

import java.util.HashSet;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fr.hj.draganddroppuzzle.R;
import fr.hj.draganddroppuzzle.elements.ItemInfo;
import fr.hj.draganddroppuzzle.utils.UtilsBitmap;

/**
 * Adapter for our items
 */
public class ListItemsAdapter extends BaseAdapter {
	private static final int ITEM_HEIGHT = 44 ;
	/**
	 * Async task to put the images in the listView
	 */
	public class ImageLoader extends AsyncTask<ItemInfo, Void, Bitmap>{
		private ViewHolder holder ;
		private int resId ;
		private Context context ;

		public ImageLoader(ViewHolder holder, Context context) {
			this.holder = holder ;
			this.context = context ;
			this.resId = (Integer) holder.mPicture.getTag();
		}

		@Override
		protected Bitmap doInBackground(ItemInfo... params) {
			//
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeResource(context.getResources(), resId, options);
			int imageHeight = options.outHeight;
			int imageWidth = options.outWidth;
			double ratio = (double)imageHeight / (double)imageWidth ;
			int width = imageWidth, height = imageHeight;
			if (imageWidth > mItemWidth) {
				// Image width too big
				width = mItemWidth ;
				height = (int) (width * ratio);
			}
			if (imageHeight > ITEM_HEIGHT) {
				// image height too big
				height = ITEM_HEIGHT ;
				width = (int) (height / ratio);
			}
			Log.d("ADAPTER", "width : "+width+", height : "+height);
			return UtilsBitmap.getScaledBitmapFromResource(context, resId, width, height);
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			holder.mPicture.setImageBitmap(result);
		}

	}

	private static class ViewHolder {
		public TextView mItemName;
		public ImageView mPicture;
	}

	private static final int COLOR_DARK = Color.argb(90, 125, 125, 125);
	private static final int COLOR_LIGHT = Color.argb(90, 200, 200, 200);

	private List<ItemInfo> mList;
	private LayoutInflater mInflater;
	private Context mContext ;
	private HashSet<Integer> mCheckedItems ;
	private int mItemWidth ;

	/**
	 * 
	 */
	public ListItemsAdapter(Context context, List<ItemInfo> list, int width) {
		this.mList = list;
		this.mInflater = LayoutInflater.from(context);
		this.mContext = context ;
		this.mCheckedItems = new HashSet<Integer>();
		this.mItemWidth = width ;
	}
	
	public void toggleChecked(int pos) {
		final Integer v = Integer.valueOf(pos);
		if (this.mCheckedItems.contains(v)) {
			this.mCheckedItems.remove(v);
		} else {
			this.mCheckedItems.add(v);
		}
		this.notifyDataSetChanged();
	}

	public boolean isChecked(int pos) {
		return this.mCheckedItems.contains(Integer.valueOf(pos)) ;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return mList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public ItemInfo getItem(int arg0) {
		return mList.get(arg0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		// If the view is not recycled
		if (convertView == null) {
			// We retrieve the layout
			convertView = mInflater.inflate(R.layout.item, null);
			holder = new ViewHolder();
			// We put the widgets of our layout in our holder
			holder.mItemName = (TextView) convertView
					.findViewById(R.id.tv_item_title);
			holder.mPicture = (ImageView) convertView
					.findViewById(R.id.iv_item_picture);
			// We put the holder as a tag in the layout
			convertView.setTag(holder);
		} else {
			// If we recycle the view, we retrieve its holder
			holder = (ViewHolder) convertView.getTag();
		}
		// In all cases, we retrieve the right rectangle information
		ItemInfo rect = getItem(position);
		// If it exists
		if (rect != null) {
			// Treat the rectangle
			holder.mItemName.setText(rect.getName());
			holder.mPicture.setTag(rect.getResourceId());
			new ImageLoader(holder,mContext).execute();
		}
		if (isChecked(position)) {
			convertView.setBackgroundColor(COLOR_DARK);
		} else {
			convertView.setBackgroundColor(COLOR_LIGHT);
		}
		return convertView;
	}

}
