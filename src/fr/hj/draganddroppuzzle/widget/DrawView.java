package fr.hj.draganddroppuzzle.widget;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import fr.hj.draganddroppuzzle.elements.ItemInfo;
import fr.hj.draganddroppuzzle.elements.MovingRect;
import fr.hj.draganddroppuzzle.elements.Rectangle;
import fr.hj.draganddroppuzzle.elements.TargetRect;

public class DrawView extends View {
	private static final String TAG = "DrawView";
	private Paint mPaintRectangle, mFilledRectangle;
	private MovingRect[] mRectanglesMoving = new MovingRect[4]; // Rectangles
																// moving
	private int NB_ROWS, NB_COLUMNS;
	private int rectId = 0; // variable to know what rectangle is being dragged
	private TargetRect[][] mTargetArray;
	private float ERROR_MARGIN = 3;

	public DrawView(Context context) {
		this(context, null);
	}

	public DrawView(Context context, AttributeSet attrs, int defStyle) {
		this(context, attrs);
	}

	public DrawView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFocusable(true); // necessary for getting the touch events

		// Paint for targets
		mPaintRectangle = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaintRectangle.setColor(Color.MAGENTA);
		mPaintRectangle.setStrokeWidth(3);
		mPaintRectangle.setStyle(Paint.Style.STROKE);

		mFilledRectangle = new Paint(Paint.ANTI_ALIAS_FLAG);
		mFilledRectangle.setColor(Color.BLUE);
		mFilledRectangle.setStyle(Paint.Style.FILL);
	}

	public void update(Context context, List<ItemInfo> infoList,
			boolean[][] array, int width, int height) {
		Log.d(TAG, "width : " + width + ", height : " + height);
		// Targets
		Point p;
		int x, y;
		int offsetX = width / 3; // 400
		int offsetY = height * 5 / 100; // 100
		NB_ROWS = array.length;
		NB_COLUMNS = array[0].length;
		mTargetArray = new TargetRect[NB_ROWS][NB_COLUMNS];

		int widthCar = width * 2/3 * 95/100 ; // the width that we want for our car
		int heightCar = height * 9/10; // The height that we want for our car
		int UNIT_WIDTH = widthCar/NB_COLUMNS ;
		int UNIT_HEIGHT = heightCar/NB_ROWS ;
		Rectangle.UNIT_WIDTH = UNIT_WIDTH ;
		Rectangle.UNIT_HEIGHT = UNIT_HEIGHT ;
		
		for (int m = 0; m < NB_ROWS; m++) {
			for (int n = 0; n < NB_COLUMNS; n++) {
				x = n * TargetRect.UNIT_WIDTH + offsetX;
				y = m * TargetRect.UNIT_HEIGHT + offsetY;
				p = new Point(x, y);
				mTargetArray[m][n] = new TargetRect(context, p, 1, 1,
						array[m][n]);
			}
		}

		// Moving
		mRectanglesMoving = new MovingRect[infoList.size()];
		Point pointTopLeft = new Point(10, 10);
		int i = 0;
		// declare each ball with the ColorBall class
		MovingRect.resetCount(); // the count being static, it does not reset
									// automatically when the activity is
									// paused/resumed, and could be incrementing
									// (we don't want it to do that, so we reset
									// it here
		for (ItemInfo itemInfo : infoList) {
			mRectanglesMoving[i] = new MovingRect(context,
					itemInfo.getResourceId(), pointTopLeft,
					itemInfo.getWidth(), itemInfo.getHeight(), null);
			i++;
		}
	}
	public void update(Context context, int width, int height) {
		Log.d(TAG, "width : " + width + ", height : " + height);
		this.setLayoutParams(new LinearLayout.LayoutParams(width,height));
		// Targets
		Point p;
		int x, y;
		int offsetX = width / 3; // 400
		int offsetY = height * 5 / 100; // 100

		int widthCar = width * 2/3 * 95/100 ; // the width that we want for our car
		int heightCar = height * 9/10; // The height that we want for our car
		int UNIT_WIDTH = widthCar/NB_COLUMNS ;
		int UNIT_HEIGHT = heightCar/NB_ROWS ;
		Rectangle.UNIT_WIDTH = UNIT_WIDTH ;
		Rectangle.UNIT_HEIGHT = UNIT_HEIGHT ;
		TargetRect current ;
		Log.d(TAG,"target resize :");
		for (int m = 0; m < NB_ROWS; m++) {
			for (int n = 0; n < NB_COLUMNS; n++) {
				current = mTargetArray[m][n];
				x = n * TargetRect.UNIT_WIDTH + offsetX;
				y = m * TargetRect.UNIT_HEIGHT + offsetY;
				p = new Point(x, y);
				current.resize(p);
			}
		}
		Log.d(TAG, "moving resize :");
		// Moving
		for (MovingRect movingRect : mRectanglesMoving) {
			// Reposition
			p = movingRect.getFirstTargetCoordinate();
			
			// Resize
			movingRect.resize(p);
		}
		invalidate();
	}

	/**
	 * Sets whether the moving rectangle at position pos should be visible or
	 * not
	 * 
	 * @param pos
	 * @param visible
	 *            true to draw it
	 */
	public void setVisible(int pos, boolean visible) {
		mRectanglesMoving[pos].setVisible(visible);
	}

	public void toggleVisible(int pos) {
		boolean nowVisible = mRectanglesMoving[pos].toggleVisible();
		if (nowVisible) {
			// It is now visible : check if it is at the right position
			checkPositionAndPlace(pos);
		} else {
			// It is now invisible : freed all resources used
			mRectanglesMoving[pos].freed();
		}
		invalidate();
	}

	/**
	 * Checks if the moving rectangle collides with a target rectangle <br>
	 * If it does, returns the coordinates for the moving rectangle to be
	 * released on <br>
	 * If not, return the initial position of the moving rectangle
	 * 
	 * @param movingRect
	 */
	private Point isCollide(MovingRect movingRect) {
		int x = 0;
		int y = 0;
		ArrayList<TargetRect> targetsToFill = new ArrayList<TargetRect>();
		boolean foundAnchor = false;
		TargetRect current = null;
		int i = 0, j = 0;
		for (i = 0; i < NB_ROWS; i++) {
			for (j = 0; j < NB_COLUMNS; j++) {
				current = mTargetArray[i][j];
				if (MovingRect.intersects(movingRect, current)
						&& current.isFillable() && !current.isFull()) {
					if (movingRect.getUnitWidth() + j <= NB_COLUMNS
							&& movingRect.getUnitHeight() + i <= NB_ROWS) {
						x = (int) current.left;
						y = (int) current.top;
						foundAnchor = true;
						break;
						// We found the anchor for our moving piece
						// now, we have to fill all the elements of the grid
						// that need to be
					} else {
						// If the piece cannot hold in the limited place between
						// the anchor point and the end of the grid, then go
						// back to initial point (it is too big)
						return new Point(movingRect.getInitX(),
								movingRect.getInitY());
					}
				}
			}
			if (foundAnchor) {
				break;
			}
		}
		if (current != null && foundAnchor) {
			int width = movingRect.getUnitWidth();
			int height = movingRect.getUnitHeight();
			Log.d(TAG, "(i,j)" + i + "," + j);
			for (int m = i; m < i + height; m++) {
				for (int n = j; n < j + width; n++) {
					if (mTargetArray[m][n].canBeFilled()) {
//						Log.d(TAG, "target : (" + m + "," + n
//								+ ") can be filled");
						targetsToFill.add(mTargetArray[m][n]);
					} else {
						return new Point(movingRect.getInitX(),
								movingRect.getInitY());
					}
				}
			}
		}

		TargetRect.fill(targetsToFill);
		movingRect.assignTarget(targetsToFill);
		return new Point(x, y);
	}

	// the method that draws the rectangles
	@Override
	protected void onDraw(Canvas canvas) {
		// canvas.drawColor(0xFFCCCCCC); //if you want another background color
		// Draw the rectangles targets (BEFORE !!)
		if (mTargetArray == null) {
			return;
		}
		for (TargetRect[] rectAr : mTargetArray) {
			for (TargetRect rect : rectAr) {
				canvas.drawRect(rect, (rect.isFillable() ? mPaintRectangle
						: mFilledRectangle));
			}
		}
		if (mRectanglesMoving == null) {
			return;
		}
		MovingRect current = null;
		// draw the moving rectangles on the canvas
		for (MovingRect rect : mRectanglesMoving) {
			if (rect.isVisible()) {
				canvas.drawBitmap(rect.getBitmap(), rect.left, rect.top, null);
				// canvas.drawRect(rect, rect.getPaint());
				if (rect.getID() == rectId) {
					current = rect;
				}
			}
		}
		if (current != null) {
			// If a rectangle is selected, it should be above, so we redraw it
			canvas.drawBitmap(current.getBitmap(), current.left, current.top,
					null);
			// canvas.drawRect(current, current.getPaint());
		}
	}

	// events when touching the screen
	public boolean onTouchEvent(MotionEvent event) {
		int eventaction = event.getAction();

		int X = (int) event.getX();
		int Y = (int) event.getY();
		switch (eventaction) {

		case MotionEvent.ACTION_DOWN: // touch down so check if the finger is on
										// a rectangle
			Log.w(TAG, "rectId = " + rectId);
			Log.e(TAG, "ACTION_DOWN : X = " + X + ", Y = " + Y);
			rectId = 0;
			Log.e(TAG, "nb rect : " + mRectanglesMoving.length);
			for (MovingRect rect : mRectanglesMoving) {
				// Check if the finger is inside a rectangle
				if (X >= rect.left - ERROR_MARGIN
						&& X <= rect.right + ERROR_MARGIN
						&& Y >= rect.top - ERROR_MARGIN
						&& Y <= rect.bottom + ERROR_MARGIN) {
//					Log.v(TAG, "inside the rectangle " + rect.getID()
//							+ " which is "
//							+ (rect.isVisible() ? "VISIBLE" : "invisible"));
					if (rect.isVisible()) {
						rectId = rect.getID();
						Log.d(TAG, "on a rectangle !!! (id = " + rectId + ")");
						rect.freed();
						break;
					}
				}
			}

			break;

		case MotionEvent.ACTION_MOVE: // touch drag with the rectangle
			// move the rectangle the same way as the finger
			if (rectId > 0) {
				mRectanglesMoving[rectId - 1].setCenter(X, Y);
			}

			break;

		case MotionEvent.ACTION_UP:
			Log.w(TAG, "rectId = " + rectId);
			// touch drop - just do things here after dropping
			if (rectId > 0) {
				Point p = isCollide(mRectanglesMoving[rectId - 1]);
				if (p != null) {
					mRectanglesMoving[rectId - 1].setX(p.x);
					mRectanglesMoving[rectId - 1].setY(p.y);
				}
			}
			rectId = 0;
			bonusEndGame();
			break;
		}
		// redraw the canvas
		invalidate();
		return true;

	}

	private void checkPositionAndPlace(int index) {
		Log.d(TAG, "checkPosition");
		rectId = mRectanglesMoving[index].getID();
		Point p = isCollide(mRectanglesMoving[rectId - 1]);
		if (p != null) {
			mRectanglesMoving[rectId - 1].setX(p.x);
			mRectanglesMoving[rectId - 1].setY(p.y);
		}
	}

	private void bonusEndGame() {
		for (TargetRect[] targetRow : mTargetArray) {
			for (TargetRect targetRect : targetRow) {
				if (targetRect.isFillable() && !targetRect.isFull()) {
					return;
				}
			}
		}
		Toast.makeText(getContext(), "Game finished, car is full!",
				Toast.LENGTH_LONG).show();
	}
}
