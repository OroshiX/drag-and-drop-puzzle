package fr.hj.draganddroppuzzle;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import fr.hj.draganddroppuzzle.adapter.ListItemsAdapter;
import fr.hj.draganddroppuzzle.elements.ItemInfo;
import fr.hj.draganddroppuzzle.widget.DrawView;

public class MainActivity extends ListActivity {
	private static final String TAG = "MainActivity";
	private static final int NB_ROWS = 7;
	private static final int NB_COLUMNS = 9;
	private boolean alreadyInitialized;
	private ListView mListView;
	private DrawView mDrawView;
	private ListItemsAdapter mAdapter;
	private Button mRetractBt;
	private boolean mIsVisibleList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mListView = getListView();
		mDrawView = (DrawView) findViewById(R.id.drawView);
		mRetractBt = (Button) findViewById(R.id.btn_retract_list);
		alreadyInitialized = false;
		mIsVisibleList = true;
		// setContentView(new DrawView(this));
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus && !alreadyInitialized) {
			Log.d(TAG, "update");
			long debut = System.currentTimeMillis();
			init();
			long duree = System.currentTimeMillis() - debut;
			Log.e(TAG, "duree init = " + duree);
			alreadyInitialized = true;
		}
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
	}

	private void init() {
		// Dummy init
		List<ItemInfo> list = new ArrayList<ItemInfo>();
		list.add(new ItemInfo("Bar", R.drawable.bar, 4, 2));
		list.add(new ItemInfo("Long", R.drawable.bol_blauw, 1, 5));
		list.add(new ItemInfo("Bar", R.drawable.ic_launcher, 3, 1));
		list.add(new ItemInfo("Little", R.drawable.bol_geel, 1, 1));
		list.add(new ItemInfo("Red", R.drawable.bol_rood, 1, 3));
		list.add(new ItemInfo("Soup", R.drawable.logo, 4, 4));
		list.add(new ItemInfo("Square", R.drawable.bar, 2, 2));
		list.add(new ItemInfo("Raquette", R.drawable.bol_groen, 2, 3));
		list.add(new ItemInfo("Sac de couchage", R.drawable.bol_paars, 6, 2));

		// array to fill with values like "fillable" or "not fillable"
		boolean[][] array = new boolean[NB_ROWS][NB_COLUMNS];
		for (int i = 0; i < NB_ROWS; i++) {
			for (int j = 0; j < NB_COLUMNS; j++) {
				array[i][j] = true;
			}
		}
		array[0][4] = false;
		array[1][4] = false;
		array[3][1] = false;
		array[4][1] = false;
		array[0][5] = false;

		View content = getWindow().findViewById(Window.ID_ANDROID_CONTENT);
		int screenWidth = content.getWidth();
		int screenHeight = content.getHeight();
		// List takes 1/5 screen, so screen width left :
		int effectiveWidth = screenWidth * 4 / 5;
		int effectiveHeight = screenHeight;

		long start = System.currentTimeMillis();
		mDrawView.update(getApplicationContext(), list, array, effectiveWidth,
				effectiveHeight);
		Log.d(TAG, "update : " + (System.currentTimeMillis() - start));
		int listWidth = screenWidth * 17 / 100 ;
		mAdapter = new ListItemsAdapter(this, list, listWidth);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				mAdapter.toggleChecked(arg2);
				mDrawView.toggleVisible(arg2);
			}
		});
		mRetractBt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				View content = getWindow().findViewById(
						Window.ID_ANDROID_CONTENT);
				int screenWidth = content.getWidth();
				int screenHeight = content.getHeight();
				if (mIsVisibleList) {
					int effectiveWidth = (int) (0.95 * screenWidth);
					int effectiveHeight = screenHeight;

					mListView.setVisibility(View.GONE);
					LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
							3 * screenWidth / 100, screenHeight);
					layoutParams.setMargins(2 * screenWidth / 100, 0, 0, 0);
					mRetractBt.setLayoutParams(layoutParams);
					mDrawView.update(MainActivity.this, effectiveWidth,
							effectiveHeight);
					mDrawView.invalidate(); // recalculate visible surface and
											// redraw!!
					mIsVisibleList = false;
				} else {
					// Proportions :
					//
					// List takes 17 % screen width
					// Button takes 3 % screen width
					// DrawView takes 80 % screen width
					int effectiveWidth = screenWidth * 4 / 5;
					int effectiveHeight = screenHeight;
					mListView.setVisibility(View.VISIBLE);
					mListView.setLayoutParams(new LinearLayout.LayoutParams(
							17 * screenWidth / 100, screenHeight));
					mRetractBt.setLayoutParams(new LinearLayout.LayoutParams(
							3 * screenWidth / 100, screenHeight));
					mDrawView.update(MainActivity.this, effectiveWidth,
							effectiveHeight);
					mIsVisibleList = true;
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
