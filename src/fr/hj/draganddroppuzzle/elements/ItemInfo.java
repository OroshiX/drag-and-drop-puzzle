package fr.hj.draganddroppuzzle.elements;

public class ItemInfo {
	private String mName;
	private int mResId;
	private int mWidth;
	private int mHeight;

	public ItemInfo(String name, int resId, int width, int height) {
		this.mName = name;
		this.mResId = resId ;
		this.mWidth = width ;
		this.mHeight = height ;
	}

	/**
	 * @return the mName
	 */
	public String getName() {
		return mName;
	}

	/**
	 * @param mName
	 *            the mName to set
	 */
	public void setName(String name) {
		this.mName = name;
	}

	/**
	 * @return the resource ID
	 */
	public int getResourceId() {
		return mResId;
	}

	/**
	 * @param mPath
	 *            the mPath to set
	 */
	public void setRessourceId(int resId) {
		this.mResId = resId;
	}

	public int getWidth() {
		return this.mWidth ;
	}
	
	public int getHeight() {
		return this.mHeight ;
	}

}
