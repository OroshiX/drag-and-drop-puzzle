package fr.hj.draganddroppuzzle.elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.RectF;

public abstract class Rectangle extends RectF {
	public static int UNIT_WIDTH = 60;
	public static int UNIT_HEIGHT = 60;
	protected Bitmap mImage; // the image of the rectangle
	protected float mWidth;
	protected float mHeight;
	protected final int mUnitWidth;
	protected final int mUnitHeight;

	protected Rectangle(Context context, int drawable, Point pointTopLeft,
			int width, int height) {
		super(pointTopLeft.x, pointTopLeft.y, pointTopLeft.x + width
				* UNIT_WIDTH, pointTopLeft.y + height * UNIT_HEIGHT);
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		Bitmap fullScale = BitmapFactory.decodeResource(context.getResources(),
				drawable);
		mUnitWidth = width;
		mUnitHeight = height;
		mWidth = width();
		mHeight = height();
		mImage = Bitmap.createScaledBitmap(fullScale, (int) mWidth,
				(int) mHeight, true);
	}

	protected Rectangle(Context context, Point pointTopLeft, int width,
			int height) {
		super(pointTopLeft.x, pointTopLeft.y, pointTopLeft.x + width
				* UNIT_WIDTH, pointTopLeft.y + height * UNIT_HEIGHT);
		mUnitWidth = width;
		mUnitHeight = height;
		mWidth = width();
		mHeight = height();
		mImage = null ;
	}
	
	public void resize(Point p) {
		this.left = p.x ;
		this.top = p.y ;
		this.right = left+UNIT_WIDTH*mUnitWidth;
		this.bottom = top+UNIT_HEIGHT*mUnitHeight;
		mWidth = width() ;
		mHeight = height() ;
	}
	public int getUnitWidth() {
		return mUnitWidth;
	}

	public int getUnitHeight() {
		return mUnitHeight;
	}

	public Bitmap getBitmap() {
		return mImage;
	}
}
