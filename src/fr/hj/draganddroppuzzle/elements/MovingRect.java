package fr.hj.draganddroppuzzle.elements;

import java.util.ArrayList;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.Log;

public class MovingRect extends Rectangle {
	private final int id; // gives every rectangle his own id, for now not
							// necessary
	private static int count = 1;
//	private boolean goRight = true;
//	private boolean goDown = true;
	private boolean mIsVisible = false;
	private final int initX;
	private final int initY;
	private ArrayList<TargetRect> targetRects;
	private Paint mPaint;

	public MovingRect(Context context, int drawable, Point pointTopLeft,
			int width, int height) {
		super(context, drawable, pointTopLeft, width, height);
		id = count;
		count++;
		initX = pointTopLeft.x;
		initY = pointTopLeft.y;
		targetRects = null;
		mIsVisible = false;
		Log.d("Moving", "count = " + count);
	}

	public MovingRect(Context context, int drawable, Point pointTopLeft,
			int width, int height, Paint paint) {
		super(context, drawable, pointTopLeft, width, height);
		id = count;
		count++;
		Log.d("MovingRect", "count = " + count);
		initX = pointTopLeft.x;
		initY = pointTopLeft.y;
		targetRects = null;
		mIsVisible = false;
		this.mPaint = paint;
	}

	public static int getCount() {
		return count;
	}

	public Paint getPaint() {
		return mPaint;
	}

	public int getID() {
		return id;
	}

	public void setX(int x) {
		right = x + width();
		left = x;
	}

	public void setY(int y) {
		bottom = y + height();
		top = y;
	}

	public int getInitX() {
		return initX;
	}

	public int getInitY() {
		return initY;
	}

	/**
	 * @return the mVisible
	 */
	public boolean isVisible() {
		return mIsVisible;
	}

	/**
	 * @param mVisible
	 *            the mVisible to set
	 */
	public void setVisible(boolean mVisible) {
		this.mIsVisible = mVisible;
	}

	public void assignTarget(ArrayList<TargetRect> targetRects) {
		this.targetRects = targetRects;
	}

	public void freed() {
		if (this.targetRects != null) {
			for (TargetRect targetRect : targetRects) {
				targetRect.setIsFull(false);
			}
			this.targetRects = null;
		}
	}

	public void setCenter(int x, int y) {
		this.offsetTo(x - mWidth / 2, y - mHeight / 2);
	}

	public static void resetCount() {
		count = 1;
	}

	/**
	 * Toggle the visibility of the rectangle
	 * 
	 * @return the new value
	 */
	public boolean toggleVisible() {
		mIsVisible = !mIsVisible;
		return mIsVisible;
	}

	public Point getFirstTargetCoordinate() {
		Point p = null;
		if (targetRects != null && targetRects.size() > 0) {
			TargetRect rect = this.targetRects.get(0);
			p = new Point((int) rect.left, (int) rect.top);
		} else {
			p = new Point(this.initX, this.initY);
		}
		return p;
	}

	@Override
	public void resize(Point p) {
		super.resize(p);
		// mImage = UtilsBitmap.createScaledBitmap(mImage, (int) mWidth,
		// (int) mHeight, UtilsBitmap.ScalingLogic.FIT);
		mImage = Bitmap.createScaledBitmap(mImage, (int) mWidth, (int) mHeight,
				true);
	}
}
