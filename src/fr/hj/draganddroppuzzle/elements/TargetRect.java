package fr.hj.draganddroppuzzle.elements;

import java.util.ArrayList;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;

/**
 * @author jess
 * 
 */
public class TargetRect extends Rectangle {
	private boolean mIsFull ; // Whether the rectangle is already filled with all it can contain or not
	private final boolean mIsFillable;
	/**
	 * Use the other one! We - normally - don't need this one
	 * @param context
	 * @param drawable
	 * @param pointTopLeft
	 * @param width
	 * @param height
	 * @param fillable
	 */
	public TargetRect(Context context, int drawable, Point pointTopLeft, int width, int height, boolean fillable) {
		super(context, drawable, pointTopLeft, width, height);
		mIsFull = false ;
		mIsFillable = fillable ;
	}
	
	/**
	 * Construct method for a target rectangle
	 * @param context
	 * @param pointTopLeft coordinates of the top left point of the rectangle
	 * @param width width in terms of number of units (in a grid)
	 * @param height height in terms of number of units (in a grid)
	 * @param fillable whether the target can be filled
	 */
	public TargetRect(Context context, Point pointTopLeft, int width, int height, boolean fillable) {
		super(context, pointTopLeft, width, height);
		mIsFull = false ;
		mIsFillable = fillable ;
	}
	
	public Bitmap getBitmap() {
		return mImage ;
	}
	
	public boolean isFull() {
		return mIsFull ;
	}
	
	/**
	 * If the rectangle is fillable, then we set it to full and return true, else return false
	 * @param full
	 * @return true if success
	 */
	public boolean setIsFull(boolean full) {
		if (mIsFillable) {
			mIsFull = full ;
			return true ;
		} else {
			return false ;
		}
	}
	public boolean canBeFilled() {
		if (mIsFillable && !mIsFull) {
			return true ;
		} else {
			return false ;
		}
	}
	
	public boolean isFillable() {
		return mIsFillable ;
	}
	/**
	 * The ability to be filled is not checked here <br>
	 * It should be checked beforehand
	 * @param targetsToFill
	 */
	public static void fill(ArrayList<TargetRect> targetsToFill) {
		for (TargetRect targetRect : targetsToFill) {
			targetRect.mIsFull = true;
		}
	}
}
